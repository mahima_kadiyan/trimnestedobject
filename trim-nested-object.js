

//removes whitespace from both ends of a string
//works for nested objects in a object
const trimObject = (obj) => {
    if (obj === null && !Array.isArray(obj) && typeof obj != 'object') return obj;
    Object.keys(obj).forEach(key => {
        if (typeof obj[key] === 'string') {
            obj[key] = obj[key].trim();
        } else  if (typeof obj[key] === 'object' && !isEmptyFun(obj[key])) { 
            //if the obj[key] is nested call trimObject function again
            trimObject(obj[key])
        }
    });
    return obj;
}
function isEmptyFun(string) {
    if (string.length === 0) {
        return true;
    } else {
        return false;
    }
}

console.log(trimObject(
    {
        ajaxsettings: { "ak1": "v1", "ak2": "v2  " },
        uisettings: { "ui1": "  v1", "ui22": "v2" }
    }
));